import { App } from '@fm/server-crm';

import { initRoute as test } from './modules/test';

export const initRoute = (app: App) => {
    test(app);
};
