import { createRender, App } from '@fm/server-crm';

export const initRoute = ({ server, renderer }: App) => {
    // 渲染中间件
    const renderMiddleware = createRender(renderer);
    // 组件测试页面
    server.get('/test', renderMiddleware);
};
