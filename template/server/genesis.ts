import { App } from '@fm/server-crm';
import { initRoute } from './routes';
export const app = new App({
    ssr: {
        build: {
            transpile: [
                /crm-lib/,
                /@fm/,
                /vue-echarts/,
                /zrender/,
                /element-ui/
            ]
        }
    }
});

app.hook.baseMiddlewareAfter(initRoute);
