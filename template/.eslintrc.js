module.exports = {
    extends: [require.resolve('@fmfe/genesis-lint')],
    rules: {
        '@typescript-eslint/no-duplicate-imports': 'off'
    }
};
