#! /bin/sh

set -e

if [ -n $NODE_ENV ]; then
  echo $NODE_ENV
else
  echo "请设置环境变量"
fi
if [ -n $DEPLOY_ENV ]; then
  echo $DEPLOY_ENV
else
  echo "请设置部署命令"
fi
cp -a ./dist/ssr-broker-crm/client-dist/* ./dist/ssr-broker-crm/client

exec npm run deploy:$DEPLOY_ENV
