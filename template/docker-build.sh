#! /bin/sh
tag=`head -1 CHANGELOG`

testSibmitCode(){
if [ -n "$(git status -s)" ];then
    echo "还有未提交代码，请先提交代码";
    exit -1
fi
}

testSibmitCode


tagLog="$(git tag -l $tag)"

if [ "$tagLog" != "" ];then
    echo "tag已存在，请定义新的tag";
    exit -1
fi
testError(){
    if [ $? -ne 0 ]; then
        rm -rf node_modules
        mv node_modules_back node_modules
        exit -1
    fi
}

yarn config set registry http://betanpm.followme-internal.com
rm -rf node_modules
rm -rf node_modules_back
yarn install
testSibmitCode
testError
yarn build:ts
testError
NODE_ENV=production node dist/node/genesis.build.js
testError
mv node_modules node_modules_back
yarn --prod
testError

img="dockerhub.followme-internal.com/deploy/ssr-broker-crm:$tag"
echo "image:$img"
testError

docker build -t $img .
testError

docker push $img
testError
docker rmi $img
testError

if [ "$tagLog" == "" ];then
    git tag $tag
    git push origin $tag
fi
rm -rf node_modules
mv node_modules_back node_modules

echo "SUCCESS!"
