import Vue from 'vue';
import { ClientOptions } from '@fmfe/genesis-core';
import { createClientApp, createClientRouter } from '@fm/client-crm';
import { App, routes } from './routes';

export default async (clientOptions: ClientOptions): Promise<Vue> => {
    return createClientApp({
        App,
        clientOptions,
        vueOptions: {
            router: createClientRouter(clientOptions, routes)
        }
    });
};
