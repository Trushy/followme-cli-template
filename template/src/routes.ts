import { RouteConfig } from 'vue-router';
import App from './app.vue';

import { routes as test } from './modules/test/routes';

export { App };
export const routes: RouteConfig[] = [
    ...test
];
