import { RenderContext } from '@fmfe/genesis-core';
import { createServerApp, createServerRouter } from '@fm/client-crm';
import Vue from 'vue';
import { App, routes } from './routes';

export default async (renderContext: RenderContext): Promise<Vue> => {
    return createServerApp({
        App,
        renderContext,
        vueOptions: {
            router: createServerRouter(renderContext, routes)
        }
    });
};
