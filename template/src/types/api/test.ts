/* eslint-disable import/export */
/* eslint-disable @typescript-eslint/no-namespace */
import { Types } from '@fm/client-crm';

declare module './index' {
    export namespace API.Test {
        export interface Params {
            msg: string;
        }

        export type Response = Types.Response.All<null>;
    }
}
