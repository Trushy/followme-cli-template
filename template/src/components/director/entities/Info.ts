/* eslint-disable @typescript-eslint/explicit-member-accessibility */
import { Summary } from 'ssr-broker-crm/src/types/summary';
import { Components } from 'ssr-broker-crm/src/types/components';

export default class Info implements Components.Director.Info {
    nickName = '- -'; // 姓名
    id = 0;
    gender: Summary.Gender = Summary.Gender.MAN; // 姓别
    avatar =
        'https://images.chinatimes.com/newsphoto/2020-07-05/1024/20200705002812.jpg'; // 头像
    member = 0; // 成员数
    status: Summary.UserStatus = Summary.UserStatus.LOGOUT;
    group = '- -'; // 组名
    joinTime = 0; // 加入时间
}
