export default {
    words: {
        member: 'Member',
        id: 'ID',
        group: 'Group',
        joined: 'Joined',
        normal: 'Active',
        disable: 'Disable',
        logout: 'Logout'

    }
}