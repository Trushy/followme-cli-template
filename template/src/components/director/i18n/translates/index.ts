import zhCN from "../zh-CN/index";
import zhTW from "../zh-TW/index";
import enUS from "../en-US/index";
import idID from "../id-ID/index";
import jaJP from "../ja-JP/index";
import koKR from "../ko-KR/index";
import msMY from "../ms-MY/index";
import thTH from "../th-TH/index";
import viVN from "../vi-VN/index";
import zuZA from "../zu-ZA/index";

declare namespace I18nMessages {
  interface _words {
    "member": "Member";
    "id": "ID";
    "group": "Group";
    "joined": "Joined";
    "normal": "Active";
    "disable": "Disable";
    "logout": "Logout";
  }

  interface Keys {
    "words": _words;
    "words.member": _words["member"];
    "words.id": _words["id"];
    "words.group": _words["group"];
    "words.joined": _words["joined"];
    "words.normal": _words["normal"];
    "words.disable": _words["disable"];
    "words.logout": _words["logout"];
  }
}

export type Keys = I18nMessages.Keys;


export const i18n = {
    messages: {
    "zh-CN": zhCN,
    "zh-TW": zhTW,
    "en-US": enUS,
    "id-ID": idID,
    "ja-JP": jaJP,
    "ko-KR": koKR,
    "ms-MY": msMY,
    "th-TH": thTH,
    "vi-VN": viVN,
    "zu-ZA": zuZA,
  }
  };

export function translate<K extends keyof Keys>(
  this: any, 
  key: K
): Keys[K] {
  return this.$t(key);
}

