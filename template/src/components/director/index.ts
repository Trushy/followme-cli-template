import Main from './main.vue';
import { Components } from 'ssr-broker-crm/src/types/components';

export interface TypeProps {
    info: Components.Director.Info;
    labelList: Components.Director.Label[];
}
export default Main;

// export interface TypeProps {}
