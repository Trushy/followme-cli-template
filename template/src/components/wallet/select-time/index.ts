import SelectTime from './main.vue';
import { TimeEnum } from 'crm-lib/src/types/time-select-options';
import { SelectItem } from 'crm-lib/src/types/index';

export default SelectTime;

const timeSelect = (currentComponent) => {
    const langs = currentComponent.langs;
    return [
        { label: langs.list.all, value: TimeEnum.All },
        { label: langs.list.today, value: TimeEnum.Today },
        { label: langs.list.yesterday, value: TimeEnum.Yesterday },
        { label: langs.list.last3dyas, value: TimeEnum.Last3days },
        { label: langs.list.last7days, value: TimeEnum.Last7days },
        { label: langs.list.thisWeek, value: TimeEnum.ThisWeek },
        { label: langs.list.lastWeek, value: TimeEnum.LastWeek },
        { label: langs.list.thisMonth, value: TimeEnum.ThisMonth },
        { label: langs.list.lastMonth, value: TimeEnum.LastMonth },
        { label: langs.list.last3Month, value: TimeEnum.Last3Month }
    ] as SelectItem[];
};

export { timeSelect };
