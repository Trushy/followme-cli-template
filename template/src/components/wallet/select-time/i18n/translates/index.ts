import zhCN from "../zh-CN/index";
import zhTW from "../zh-TW/index";
import enUS from "../en-US/index";
import idID from "../id-ID/index";
import jaJP from "../ja-JP/index";
import koKR from "../ko-KR/index";
import msMY from "../ms-MY/index";
import thTH from "../th-TH/index";
import viVN from "../vi-VN/index";
import zuZA from "../zu-ZA/index";

declare namespace I18nMessages {
  interface _words_list {
    "all": "ALL";
    "today": "Today";
    "yesterday": "Yesterday";
    "last3dyas": "Last 3 days";
    "last7days": "Last 7 days";
    "thisWeek": "This Week";
    "lastWeek": "Last Week";
    "thisMonth": "This Month";
    "lastMonth": "Last Month";
    "last3Month": "Last 3 Month";
  }

  interface _words {
    "list": _words_list;
  }

  interface Keys {
    "words": _words;
    "words.list": _words_list;
    "words.list.all": _words_list["all"];
    "words.list.today": _words_list["today"];
    "words.list.yesterday": _words_list["yesterday"];
    "words.list.last3dyas": _words_list["last3dyas"];
    "words.list.last7days": _words_list["last7days"];
    "words.list.thisWeek": _words_list["thisWeek"];
    "words.list.lastWeek": _words_list["lastWeek"];
    "words.list.thisMonth": _words_list["thisMonth"];
    "words.list.lastMonth": _words_list["lastMonth"];
    "words.list.last3Month": _words_list["last3Month"];
  }
}

export type Keys = I18nMessages.Keys;


export const i18n = {
    messages: {
    "zh-CN": zhCN,
    "zh-TW": zhTW,
    "en-US": enUS,
    "id-ID": idID,
    "ja-JP": jaJP,
    "ko-KR": koKR,
    "ms-MY": msMY,
    "th-TH": thTH,
    "vi-VN": viVN,
    "zu-ZA": zuZA,
  }
  };

export function translate<K extends keyof Keys>(
  this: any, 
  key: K
): Keys[K] {
  return this.$t(key);
}

