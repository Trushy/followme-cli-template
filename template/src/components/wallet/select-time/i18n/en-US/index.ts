export default {
    "words": {
        "list": {
            "all": "ALL",
            "today": "Today",
            "yesterday": "Yesterday",
            "last3dyas": "Last 3 days",
            "last7days": "Last 7 days",
            "thisWeek": "This Week",
            "lastWeek": "Last Week",
            "thisMonth": "This Month",
            "lastMonth": "Last Month",
            "last3Month": "Last 3 Month"
        }
    }
}