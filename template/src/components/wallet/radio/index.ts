import FmRadio from './main.vue';

export type RadioValue = Array<string | number | boolean>;
export interface ReviewLabelValue {
    label: string;
    value: string | number;
    class?: string;
    isReject: boolean;
}

export default FmRadio;
