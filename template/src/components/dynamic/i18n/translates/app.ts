import zhCN from "../zh-CN/app";
import zhTW from "../zh-TW/app";
import enUS from "../en-US/app";
import idID from "../id-ID/app";
import jaJP from "../ja-JP/app";
import koKR from "../ko-KR/app";
import msMY from "../ms-MY/app";
import thTH from "../th-TH/app";
import viVN from "../vi-VN/app";
import zuZA from "../zu-ZA/app";

declare namespace I18nMessages {
  interface _list {
    "all": "ALL";
    "today": "Today";
    "yesterday": "Yesterday";
    "last3dyas": "Last 3 days";
    "last7days": "Last 7 days";
    "thisWeek": "This Week";
    "lastWeek": "Last Week";
    "thisMonth": "This Month";
    "lastMonth": "Last Month";
    "last3Month": "Last 3 Month";
  }

  interface Keys {
    "totalCount": "Total: {totalCount} items ";
    "list": _list;
    "list.all": _list["all"];
    "list.today": _list["today"];
    "list.yesterday": _list["yesterday"];
    "list.last3dyas": _list["last3dyas"];
    "list.last7days": _list["last7days"];
    "list.thisWeek": _list["thisWeek"];
    "list.lastWeek": _list["lastWeek"];
    "list.thisMonth": _list["thisMonth"];
    "list.lastMonth": _list["lastMonth"];
    "list.last3Month": _list["last3Month"];
  }

  interface Values {
    "totalCount": Record<"totalCount", string>;
  }
}

export type Keys = I18nMessages.Keys;
export type Values = I18nMessages.Values;


export const i18n = {
    messages: {
    "zh-CN": zhCN,
    "zh-TW": zhTW,
    "en-US": enUS,
    "id-ID": idID,
    "ja-JP": jaJP,
    "ko-KR": koKR,
    "ms-MY": msMY,
    "th-TH": thTH,
    "vi-VN": viVN,
    "zu-ZA": zuZA,
  }
  };

export function translate<K extends keyof Keys>(
  this: any, 
  key: K,
  values?: K extends keyof Values ? Values[K] : any
): Keys[K] {
  return this.$t(key, values);
}

