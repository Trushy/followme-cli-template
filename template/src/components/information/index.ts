import Main from './main.vue';
import { TypeProps as LabelValueItem } from 'crm-lib/src/components-base/label-value';

export default Main;
export interface TypeProps {
    title?: string;
    infoList?: LabelValueItem[];
}

/**
 * $emit change
 */
export interface EmitProps {
    field: string;
    value: string | boolean;
}
